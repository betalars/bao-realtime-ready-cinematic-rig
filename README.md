# Bao - realtime cinematic rig

This is Bao, not only a neat Dungeons and Dragons character (he is a Bard/Fighter Multiclass and I think that is pretty cool) but also a great cinematic yet also realtime-ready Rig.

## Features:
 - many simple yet powerful master-controllers to quickly get big movements without the need to pose many bones.
 - driver based switches for IK/FK and other optional rig features
     - just select one of the purple bones and adjust the costum properties
 - Inverse Kinematics and Forward Kinematics for both arms and legs.
 - stretchy and non-stretchy IK
 - an axe, that can be ...
    - pinned to the back
    - work as an IK target
    - be parented to left and right arm FK chain
 - a powerful face rig, that only relies on bone deformations (and could also use a bit more improvement)
 - an extensive control righ with in 145 controllers and ~350 bones in total!
 - a real time-ready deform rig with 200 bones

## Can I use it?
Yes, this project uses a slightly modified CC-BY-SA 4.0 lisence. To put it into brief, human-understandable words you can ...
 - redistribute
 - remix and transform
 this project, as long as you
 - give appropriate creedit and [link the lisence](https://creativecommons.org/licenses/by-sa/4.0/)
 - use the same or a [compatible lisence](https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses)

There is one extension and one restiction:
 - *Extended Compatability:*
   When you want to choose a lisence for your *derivative work*, I consider any lisence compatible, that ...
     - allows others to redistribute and remix your work for at least non-commercial purposes
     - requires, that I get appropriate credit
 - *Restricted commercial use:*
     - You may not sell this on it's own.
 
 ## How to use it?
  1. Clone or Download the Project.
  2. Open it in Blender!
  3. Do your animations using the control rig
  4. Bake the actions within the deform rig -> apply constraints without removing them by selecting the appropriate baking parameters
  5. Export deform rig and character to the engine of your choice


  
